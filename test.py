# -*- coding: utf-8 -*
import os  
import time
# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_side_bar_test():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1
# 2. [Screenshot] Side Bar Text
def test_screenshot_side_bar_text():
    os.system('adb shell screencap -p /sdcard/side_bar_text.png && adb pull /sdcard/side_bar_text.png')
# 3. [Context] Categories
def test_categories():
	os.system('adb shell input tap 951 373 && adb shell input touchscreen swipe 795 1030 795 517 500 && adb shell input tap 1007 782 && adb shell input tap 992 1121 && adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .') 
	f = open('window_dump.xml', 'r', encoding="utf-8")   
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
# 4. [Screenshot] Categories
def test_screenshot_cateories():
	os.system('adb shell screencap -p /sdcard/categories.png && adb pull /sdcard/categories.png')
# 5. [Context] Categories page
def test_categories_page():
	os.system('adb shell input tap 129 1716 && adb shell input tap 129 1716 && adb shell input tap 326 1722')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('3C') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('NB') != -1

# 6. [Screenshot] Categories page
def test_screenshot_cateories_page():
    os.system('adb shell input tap 326 1722 && adb shell screencap -p /sdcard/categories_page.png && adb pull /sdcard/categories_page.png')
# 7. [Behavior] Search item “switch”
def test_search_item():
    os.system('adb shell input tap 100 100')
    time.sleep(1)
    os.system('adb shell input text "switch"')
    os.system('adb shell input keyevent "KEYCODE_ENTER"')
# 8. [Behavior] Follow an item and it should be add to the list
def test_8():
    time.sleep(1)	
    os.system('adb shell input tap 704 617')
    time.sleep(2)
    os.system('adb shell input tap 118 1714')
    time.sleep(1)
    os.system('adb shell input tap 986 135')
    time.sleep(1)	
    os.system('adb shell input tap 913 387')	
    time.sleep(1)
    os.system('adb shell input tap 100 100')
    time.sleep(2)
    os.system('adb shell input tap 420 870')
    time.sleep(1)
# 9. [Behavior] Navigate tto the detail of item
def test_9():
    os.system('adb shell input tap 273 827')
    time.sleep(1)
    os.system('adb shell input touchscreen swipe 731 1107 696 256 500')
    time.sleep(1)
    os.system('adb shell input tap 537 139')
    time.sleep(3)
    os.system('adb shell uiautomator dump sdcard/window_dump.xml && adb pull /sdcard/window_dump.xml .')
    f = open('window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('商品特色') != -1

# 10. [Screenshot] Disconnetion Screen
def test_10():
    time.sleep(1)
    os.system('adb shell input tap 986 135')
    time.sleep(1)	
    os.system('adb shell input tap 913 387')
    time.sleep(1)
    os.system('adb shell input tap 531 1720')
    time.sleep(3)
    
    os.system('adb shell svc wifi disable')	
    os.system('adb shell svc data disable')	
    time.sleep(10)       	

    os.system('adb shell screencap -p /sdcard/disconnect.png')
    os.system('adb pull /sdcard/disconnect.png')
    os.system('adb shell svc wifi enable')	
    os.system('adb shell svc data enable')		
    